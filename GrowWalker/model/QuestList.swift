//
//  QuestList.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//
//  Require Attributes :
//  @NSManaged var no: NSNumber?
//  @NSManaged var body: String?
//  @NSManaged var title: String?
//  @NSManaged var prev: NSNumber?
//  @NSManaged var rewardGold: NSNumber?
//  @NSManaged var rewardItems: String?
//  @NSManaged var rewardEnergy: NSNumber?
//  @NSManaged var clear: NSNumber?
//  @NSManaged var current: NSNumber?
//  @NSManaged var target: NSNumber?
//  @NSManaged var criteria: String?
//

import Foundation


class QuestList {
    let formatter = NSNumberFormatter()
    var questDict = [[NSDictionary]]()
    var dictionary = [String: String]()
    var itemArray:[[String: String]] = [
    ["body":"","title":"","prev":"0","gold":"0","items":"","energy":"0","current":"0","target":"0","criteria":""],
        ["body":"식물을 10개 수집하세요","title":"수집(1)","prev":"0","gold":"1000","items":"","energy":"0","current":"0","target":"10","criteria":"collection"],
        ["body":"식물을 1개 수확하세요","title":"수확(1)","prev":"0","gold":"1000","items":"","energy":"0","current":"0","target":"1","criteria":"crop"],
        ["body":"식물을 10개 수확하세요","title":"수확(2)","prev":"0","gold":"1000","items":"","energy":"0","current":"0","target":"10","criteria":"crop"],
        ["body":"식물을 20개 수확하세요","title":"수확(3)","prev":"0","gold":"1000","items":"","energy":"0","current":"0","target":"20","criteria":"crop"],
        ["body":"식물을 30개 수확하세요","title":"수확(4)","prev":"0","gold":"1000","items":"","energy":"0","current":"0","target":"30","criteria":"crop"]
    ]
    func getDictionary() -> NSDictionary {
        var dict: [Int: NSDictionary] = [:]
        for no in 0 ..< itemArray.count{
            let noString = formatter.stringFromNumber(no)
            itemArray[no]["no"] = noString
            dict[no] = NSDictionary.init(dictionary: itemArray[no])
        }
        return NSDictionary.init(dictionary: dict)
    }
    
    func getArray() -> [[String: String]] {
        return itemArray
    }
    
}
//
//  GroundService.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class GroundService {
    
    let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext

    // Saves all changes
    func saveChanges(){
        do{
            try context.save()
        } catch let error as NSError {
            // failure
            print(error)
        }
    }
    
    func create(position: NSNumber, hasGrown: NSNumber, startDate: NSDate) -> Ground {
        
        let newItem = NSEntityDescription.insertNewObjectForEntityForName(Ground.entityName, inManagedObjectContext: context) as! Ground
        
        newItem.position = position
        newItem.hasGrown = hasGrown
        newItem.startDate = NSDate()
        newItem.nutrients = false
        
        self.saveChanges()
        return newItem
    }
    
    // Gets a Status by id
    func getById(id: NSManagedObjectID) -> Ground? {
        return context.objectWithID(id) as? Ground
    }
    
    
    func get(ground:NSNumber, position:NSNumber) -> Ground {
        
        let predicate = NSPredicate.init(format: "position = %@", position)
        let fetchRequest = NSFetchRequest(entityName: Ground.entityName)
        
        fetchRequest.predicate = predicate
        do {
            let response = try context.executeFetchRequest(fetchRequest)
            if(response.count<1){
                return create(position, hasGrown:0, startDate:NSDate())
            } else {
                return response.last as! Ground
            }
            
        } catch let error as NSError {
            // failure
            print(error)
            return Ground()
        }
        
    }
    
    func getAll() -> [Ground]{
        return getWith(withPredicate: NSPredicate(value:true))
    }
    
    func getWith(withPredicate queryPredicate: NSPredicate) -> [Ground]{
        let fetchRequest = NSFetchRequest(entityName: Ground.entityName)
        
        fetchRequest.predicate = queryPredicate
        
        do {
            let response = try context.executeFetchRequest(fetchRequest)
            return response as! [Ground]
            
        } catch let error as NSError {
            // failure
            print(error)
            return [Ground]()
        }
    }
    
    // Updates a Status
    func update(updatedStatus: Ground){
        if let Ground = getById(updatedStatus.objectID){
            Ground.position = updatedStatus.position
            Ground.hasGrown = updatedStatus.hasGrown
            Ground.startDate = NSDate()
        }
    }
    
    // Deletes a Status
    func deleteStatus(id: NSManagedObjectID){
        if let statusToDelete = getById(id){
            context.deleteObject(statusToDelete)
        }
    }
}
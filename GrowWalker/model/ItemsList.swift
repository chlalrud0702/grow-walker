//
//  ItemsList.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//
//  Requred Attributes
//  @NSManaged var no: NSNumber?
//  @NSManaged var name: String?
//  @NSManaged var desc: String?
//  @NSManaged var cost: NSNumber?

import Foundation


class ItemsList {
    
    var itemsDict = [Int: Array<String>]()
    var itemArray:[[String: String]] = [
    ["name":"영양제", "desc":"식물이 빠르고 건강하게 자랄 수 있도록 도와주는 고급 영양제.","cost":"5000"],
    ["name":"살수기", "desc":"땅이 마르지 않도록 자동으로 물을 뿌려주는 기계. 물탱크가 비지 않도록 채워줘야 한다.","cost":"3000000"]
    ]

    func getArray() -> [[String: String]] {
        return itemArray
    }
    
}
//
//  ViewController.swift
//  GrowWalker
//
//  Created by Myongji on 2016. 6. 30..
//  Copyright © 2016년 Myongji. All rights reserved.
//

import UIKit

class MenuView: UIViewController {
    
    @IBOutlet weak var Achieve: UIButton!
    @IBOutlet weak var Quest: UIButton!
    @IBOutlet weak var Collection: UIButton!
    @IBOutlet weak var Diary: UIButton!
    @IBOutlet weak var Option: UIButton!
    
    @IBOutlet weak var SideBar: UIImageView!
    @IBOutlet weak var GoToMain: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clearColor()
        view.opaque = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

